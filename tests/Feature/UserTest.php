<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Models\User;

use Faker\Factory as Faker;

class UserTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * testIndexUser - test index method
     */
    public function testIndexUser()
    {
        $user = User::orderBy('id','desc')->first();

        $response = $this->get('/users');
        $response->assertSeeText($user->name);
        $response->assertSeeText($user->email);
    }

    /**
     * testStoreUser - test store method
     */
    public function testStoreUser()
    {
        $faker = Faker::create();

        $user = [
            'name'=>$faker->name,
            'email'=>$faker->unique()->safeEmail,
            'password'=>'password',
            'password_confirmation'=>'password',
            'roles'=>''
        ];

        $response = $this->post('/users',$user);

        $response->assertStatus(302);
        $response->assertRedirect('/users');
        $response->assertSessionHas('flash_message','User successfully added.');

        $this->assertDatabaseHas((new User)->getTable(), [
            'name'=>$user['name'],
            'email'=>$user['email']
        ]);
    }

    /**
     * testStoreInvalidUser - test store method
     */
    public function testStoreInvalidUser()
    {
        $faker = Faker::create();

        $user = [
            'test_string',
            'test_string'=>'test_string',
            'name'=>$faker->name,
            'email'=>$faker->unique()->safeEmail,
            'password'=>'password',
            'password_confirmation'=>'password',
            'roles'=>''
        ];

        $response = $this->post('/users',$user);

        $response->assertStatus(302);
        $response->assertRedirect('/users');
        $response->assertSessionHas('flash_message','User successfully added.');

        $this->assertDatabaseHas((new User)->getTable(), [
            'name'=>$user['name'],
            'email'=>$user['email']
        ]);
    }

    /**
     * testEditUser - test edit method
     */
    public function testEditUser()
    {
        $user = User::orderBy('id')->first();
        $response = $this->get("/users/{$user->id}/edit");
        $response->assertSeeText("Edit {$user->name}");
    }

    /**
     * testUpdateUser - test update method
     */
    public function testUpdateUser()
    {
        $user = User::orderBy('id','desc')->first();

        $faker = Faker::create();
        $fakerUser = [
            'name'=>$faker->name,
            'email'=>$faker->unique()->safeEmail,
            'password'=>'password',
            'password_confirmation'=>'password',
        ];
        $response = $this->put("/users/{$user->id}", $fakerUser);

        $response->assertStatus(302);
        $response->assertRedirect('/users');
        $response->assertSessionHas('flash_message','User successfully edited.');

        $this->assertDatabaseHas((new User)->getTable(), [
            'id'=>$user->id,
            'name'=>$fakerUser['name'],
            'email'=>$fakerUser['email']
        ]);
    }

    /**
     * testDeleteUser - test delete method
     */
    public function testDeleteUser()
    {
        $user = User::orderBy('id','desc')->first();

        $response = $this->delete("/users/{$user->id}");

        $response->assertStatus(302);
        $response->assertRedirect('/users');
        $response->assertSessionHas('flash_message','User successfully deleted.');

        $this->assertDatabaseMissing((new User)->getTable(), [
            'id'=>$user->id
        ]);
    }
}
